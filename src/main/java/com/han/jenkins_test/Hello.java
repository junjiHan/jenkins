package com.han.jenkins_test;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Hello {
    @Value("${server.port}")
    private String port;
    @RequestMapping("/hello")
    public String hello(){
        return "hello";
    }

    @RequestMapping("/port")
    public String port(){
        return port;
    }

}
